<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" style="max-width: 600px; width: 100%; margin:0 auto; text-align: center;">
	<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>*|MC:SUBJECT|*White Rabbit Group</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<style> 
			* {
	          -ms-text-size-adjust: 100%;
	          -webkit-text-size-adjust: 100%;
	        }
            body {
                margin: 0 !important;
                padding: 0 !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important; 
            }

            .employee_table, .employee_table th, .employee_table td {
                border: 1px solid;
                padding: 5px;
            }

            .employee_table td, .employee_table th {
                text-align: center;
            }

            @media screen and (max-width: 580px) {
            	.content-class {
            	    padding-left: 5px;
                    padding-right: 5px;
                }
            }
		</style>
		<!--[if gte mso 9]>
			<xml>
				<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
				</o:OfficeDocumentSettings>
			</xml>
		<![endif]-->
	</head>
	<body style="background-color: #F3F3F3">
		<table style="max-width: 600px; width: 100%; margin:0 auto; " role="presentation">
			<tr>
				<td>
					<table style="margin:0 auto; padding-top: 27px; background-color: #F3F3F3; width: 100%;" role="presentation">
						<tr>
							<td style="text-align: center; padding: 0;">
								<a href="https://whiterabbit.group/" target="_blank"><img src="https://whiterabbit.group/newsletter/rabbit.jpg" alt="whiterabbit.group" style="display: block; margin: 0 auto; width: 67.64px; border: 0;" width="67.64" height="67.64"></a>
							</td>
						</tr>
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif; font-weight: 300; font-size: 17px; line-height: 23.15px; color: #000000; text-align: center; margin:0; padding-top: 30.36px; padding-bottom: 30px;">
                                <p style="font-family: 'Open Sans', sans-serif; font-weight: 300; font-size: 17px; line-height: 23.15px; color: #000000; text-align: center; margin: 0;">Time Log Report of {{ Carbon\Carbon::parse($date)->format('l, j F') }}</p>
                            </td>
                        </tr>
					</table>
				</td>
			</tr>
            <tr style="margin-bottom: 20px">
                <td>
                    <table style="background-color: #FFFFFF; width: 100%; border-top: 2px solid #000000; margin: 0 auto;" role="presentation">
                        <tr>
                            <td style="font-family: 'Raleway', sans-serif; font-weight: 700; font-size: 14px; line-height: 18.19px; letter-spacing: 2px; text-align: left; margin: 0; padding: 60px 8% 0 8%;">
                                @foreach ($teams as $team)
                                    <table>
                                        <tr>
                                            <td>
                                                <h1 style="color: #000000; font-family: 'Raleway', sans-serif; font-weight: 700; line-height: 18.19px; letter-spacing: 2px; text-align: left; margin: 0;">{{ $team->name }}</h1>
                                            </td>
                                        </tr>                                
                                    </table>
                                    <table class="employee_table" style="border-collapse:collapse; margin: 20px 0;">
                                        <tr>
                                            <th style="font-size: 12 px; width: 85%;">Employee Name</th>
                                            <th style="font-size: 12 px;">Hours</th>
                                        </tr>
                                        @foreach ($team->users as $user)
                                            <tr>
                                                <td style="font-weight: 300; font-size: 12 px;">{{ $user->name }}</td>
                                                <td style="font-weight: 300; font-size: 12 px;">{{ $user->total_hours }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endforeach
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="margin: 0 auto; width: 100%; background-color: #F3F3F3; padding-top: 40px; text-align: center;" role="presentation"> 
                        <tr>
                            <td style="padding-bottom: 30px; margin: 0;">
                                <a href="https://whiterabbit.group/" target="_blank"><img src="https://whiterabbit.group/newsletter/outline.png" alt="whiterabbit.group" style="width: 66px; border: 0;" width="66" height="75"></a>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 18px;">
                                <table style="margin: 0 auto;">
                                    <tr>
                                        <td><a href="https://www.linkedin.com/company/white-rabbit-group-inc" target="_blank"><img src="https://whiterabbit.group/newsletter/linkedin.jpg" alt="LinkedIn" style="width: 34px; border:0;" width="34"></a></td>
                                        <td style="padding-left: 17px;"><a href="https://www.instagram.com/whiterabbit.group" target="_blank"><img src="https://whiterabbit.group/newsletter/instagram.jpg" alt="Instagram" style="width: 34px; border:0;" width="34"></a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p style="font-family: 'Open Sans', sans-serif; font-weight: 400; font-size: 12px; line-height: 28px; color: rgba(0, 0, 0, 0.3); text-align: center; padding-bottom: 5px; margin: 0;">Copyright © {{ Carbon\Carbon::now()->format('Y') }} White Rabbit Group Inc.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 39px;">
                                <span style="color: #0000004D; text-decoration: underline;"><a href="*|ARCHIVE|*" style="font-family: 'Open Sans', sans-serif; font-weight: 400; font-size: 12px; line-height: 16.34px; color: rgba(0, 0, 0, 0.3) !important; text-align: center; margin: 0;">View in Browser</a></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
		</table>
	</body>
</html>