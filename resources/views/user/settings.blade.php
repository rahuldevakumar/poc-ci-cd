@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">User Settings</h3>
                    </div>
                    <!-- /.card-header -->
                    <x-auth-validation-errors class="mb-4 alert alert-danger" :errors="$errors" />
                    <!-- form start -->
                    {{ Form::model(Auth::user(), array('route' => array('user.settings.update'))) }}
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Name <span class="required"> * </span></label>
                                <div class="col-sm-9">
                                    {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Name', 'required' => 'required']) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label">Password</label>
                                <div class="col-sm-9">
                                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password']) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password_confirmation" class="col-sm-3 col-form-label">Confirm Password</label>
                                <div class="col-sm-9">
                                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation', 'placeholder' => 'Retype Password']) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Portal API Token <span class="required"> * </span></label>
                                <div class="col-sm-9">
                                    {{ Form::textarea('portal_api_key', old('portal_api_key'), ['class' => 'form-control', 'id' => 'portal_api_key', 'placeholder' => 'Portal API key', 'required' => 'required', 'cols' => '20', 'rows' => '5', 'readonly' => 'readonly']) }}
                                    <i class="fas fa-unlock" id="enable_api_key"></i>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <a href="{{ route('dashboard') }}">
                                <button type="button" class="btn btn-danger">Cancel</button>
                            </a>
                        </div>
                        <!-- /.card-footer -->
                    {{ Form::close() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $("#enable_api_key").click(function(e){
                $("#portal_api_key").prop('readonly', false);
                $(this).toggle();
            })
        })
    </script>
@endsection