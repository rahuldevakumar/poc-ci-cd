@foreach ($tag_grouping_keys as $key)
    @if (isset($key->tags))
        @php $addProjectTagGroupingKeyTitle = false; $projectCounter = 1; @endphp
        @foreach ($key->tags as $tag)
            @foreach ($tag->projects as $project)
                @if ($project->tasks)
                    @php $addProjectTitle = false; $taskCounter = 'a'; @endphp
                    @foreach ($project->tasks as $task)
                        @if (! $addProjectTagGroupingKeyTitle)
                            <h3>{{ $key->name }}</h3>
                            @php $addProjectTagGroupingKeyTitle = true; @endphp
                        @endif
                        @if (! $addProjectTitle)
                            <h4>
                                {{ $projectCounter++ }}. &nbsp;{{ $project->name }}
                                @if (isset($project->category) && isset($project->category->name))
                                ({{ $project->category->name }})
                                @endif
                            </h4>
                            @php $addProjectTitle = true; @endphp
                        @endif
                            <p class="task_item">
                            {{ $taskCounter++ }}. &nbsp;<a href="https://portal.whiterabbit.group/#/tasks/{{ $task->task_id }}">https://portal.whiterabbit.group/#/tasks/{{ $task->task_id }}</a> ( {{ $task->taskPriority }}
                            @if ($task->due_date)
                                - Due {{ $task->taskDate }} 
                            @endif
                            )
                            @if ($task->taskTags->contains('name', 'Estimate First'))
                                <b>Estimate</b>
                            @endif
                            </p>
                    @endforeach
                @endif
            @endforeach
        @endforeach
    @endif
@endforeach