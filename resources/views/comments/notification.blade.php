@extends('layouts.app')

@section('content')
<div class="projects-section notification-section" id="projects-block">
    <div class="projects-section-header projects-section-header_inline">
        <p class="custom-notification__text project-section-header__title">Notifications</p>
        <p class="custom-notification__text time dashboard__time"><?php echo Carbon\Carbon::now()->format('M, d') ?></p>
    </div>
    <div class="project-boxes jsListView" id="project-list-box">
        @if (sizeof($comments) != 0)
            <table>
                <thead>
                    <tr>
                        <th>PROJECT</th>
                        <th>TASK</th>
                        <th>COMMENT</th>
                        <th>DATE TIME</th>
                        <th>MARK AS READ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($comments as $comment)
                        <tr id="comment_{{ $comment->comment_id }}">
                            <td><h5><b>{{ $comment->task->project->name }}</b></h5></td>
                            <td><a href="{{ config('portal.api_url') }}/#/tasks/{{ $comment->task->task_id }}" target="_blank">{{ $comment->task->name }}</a></td>
                            <td>{!! $comment->body !!}</td>
                            <td>{{ \Carbon\Carbon::parse($comment->datetime)->format('j M, h:i A') }}</td>
                            <td><input type="checkbox" class="notification__read-checkbox" comment-id="{{ $comment->comment_id }}" style="width: 100px;"></td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        @else
            <h4 class="comment__message">No Comments to be viewed now</h4>
        @endif
    </div>
</div>
<script>
    $(document).ready(function() {
        $("input.notification__read-checkbox").live('click', function() {   
            var comment_id = $(this).attr('comment-id');
            $.ajax({
                type: "POST",
                url: "/delete-comment",
                data: {
                    "comment_id": comment_id,
                    "_token": "{{ csrf_token() }}"
                },
                global: false,
                async:false,
                success: function(data) {
                    $('tr#comment_' + comment_id).remove();
                }
            });
        });
    });

</script>
@endsection
