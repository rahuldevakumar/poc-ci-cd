{{-- <!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Day Planner</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var modeSwitch = document.querySelector('.mode-switch');

            modeSwitch.addEventListener('click', function () {
                document.documentElement.classList.toggle('dark');
                modeSwitch.classList.toggle('active');
            });

            var listView = document.querySelector('.list-view');
            var gridView = document.querySelector('.grid-view');
            var projectsList = document.querySelector('.project-boxes');

            listView.addEventListener('click', function () {
                gridView.classList.remove('active');
                listView.classList.add('active');
                projectsList.classList.remove('jsGridView');
                projectsList.classList.add('jsListView');
            });

            gridView.addEventListener('click', function () {
                gridView.classList.add('active');
                listView.classList.remove('active');
                projectsList.classList.remove('jsListView');
                projectsList.classList.add('jsGridView');
            });

            document.querySelector('.messages-btn').addEventListener('click', function () {
                document.querySelector('.messages-section').classList.add('show');
            });

            document.querySelector('.messages-close').addEventListener('click', function () {
                document.querySelector('.messages-section').classList.remove('show');
            });
        });
    </script>
</head>

<body body translate="no" data-new-gr-c-s-check-loaded="14.1012.0" data-gr-ext-installed="">
    <div class="app-container">
        <div class="app-header">
            <div class="app-header-left">
                <img class="logo_img" src="assets/img/logo.jpg" alt="Whiterabbit Logo" height="40" width="40">
                <p class="app-name">
                    <?php
                        $hour = date('G');
                        if ($hour >= 5 && $hour <= 11) {
                            echo "Good Morning";
                        } else if ($hour >= 12 && $hour <= 18) {
                            echo "Good Afternoon";
                        } else if ($hour >= 19 || $hour <= 4) {
                            echo "Good Evening";
                        }
                    ?>
                </p>
                <div class="search-wrapper">
                    <input class="search-input" type="text" placeholder="Search">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor"
                        stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="feather feather-search"
                        viewBox="0 0 24 24">
                        <defs></defs>
                        <circle cx="11" cy="11" r="8"></circle>
                        <path d="M21 21l-4.35-4.35"></path>
                    </svg>
                </div>
            </div>
            <button class="messages-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-message-circle">
                    <path
                        d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z" />
                    </svg>
            </button>
        </div>
        <div class="app-content">
            <div class="app-sidebar">
            </div>
            <div class="projects-section">
                <div class="project-boxes jsGridView">
                    <div class="project-box-wrapper login_form">
                        <div class="project-box" style="background-color: #fee4cb;">
                            <div class="project-box-content-header">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html> --}}


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AdminLTE 3 | Dashboard</title>

        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    
        <!-- iCheck -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css">
    
        <!-- Theme style -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.1.0/css/adminlte.min.css">
        
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">

            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="assets/img/logo.jpg" alt="Whiterabbit Logo" height="60" width="60">
            </div>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper login_wrapper">
                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
                <!-- /.content -->
            </div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

        <!-- jQuery UI 1.11.4 -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

        <!-- Bootstrap 4 -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.1.0/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
    </body>
</html>
