1. Go to the page showing Apps with access to your account: https://myaccount.google.com/u/0/permissions.
2. Under the Third-party apps menu, choose 'WRG Teamwork' App.
3. Click Remove access and then click Ok to confirm