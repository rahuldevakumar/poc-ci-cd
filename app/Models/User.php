<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'portal_api_key',
        'avatar',
        'portal_user_id',
        'username',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function googleAccessToken () {
        return $this->hasOne(GoogleAccessToken::class);
    }

    public function teams() {
        return $this->belongsToMany(Team::class);
    }

    public function timeEntries () {
        return $this->hasMany(TimeEntry::class);
    }

    public function comments () {
        return $this->hasMany(Comment::class);
    }

    public function hours () {
        return $this->hasMany(UserHour::class);
    }
}
