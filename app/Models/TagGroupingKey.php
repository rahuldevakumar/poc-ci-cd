<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TagGroupingKey extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['name', 'order'];

    public function tags () {
        return $this->hasMany(Tag::class);
    }

    public function projects() {
        return $this->tags()->with(['projects','projects.tasks']);
    }
}
