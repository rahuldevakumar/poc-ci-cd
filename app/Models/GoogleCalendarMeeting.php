<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class GoogleCalendarMeeting extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','unique_event_id','unique_calendar_id','summary','html_link','organizer_email','start','end'];

    // public function setStartAttribute($value) {
    //     $this->attributes['start'] = Carbon::parse($value, Auth::user()->timezone)->setTimezone('UTC')->format('H:i');
    // }

    // public function setEndAttribute($value) {
    //     $this->attributes['end'] = Carbon::parse($value, Auth::user()->timezone)->setTimezone('UTC')->format('H:i');
    // }
}
