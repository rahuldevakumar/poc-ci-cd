<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskAssignee extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'portal_user_id',
        'portal_task_id',
        'to_do_flag'
    ];

    /**
     * Get the task.
     */
    public function task()
    {
        return $this->belongsTo('App\Models\Task', 'task_id');
    }

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User', 'portal_user_id');
    }
}
