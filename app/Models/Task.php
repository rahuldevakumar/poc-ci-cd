<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'task_id',
        'name',
        'project_id',
        'priority',
        'status',
        'due_date',
        'is_completed',
        'description',
        'time_spent_in_mins',
        'estimated_time_in_mins',
        'details',
        'last_modified_at',
        'progress',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'array',
    ];

     /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'progress',
        'taskPriority',
        'taskDate',
    ];

    /**
     * Get the assingees for the task.
     */
    public function assignees()
    {
        return $this->belongsToMany(TaskAssignee::class, 'task_assignees');
    }

    public function taskTags()
    {
        return $this->belongsToMany(TaskTag::class, 'task_task_tag');
    }
    
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'project_id');
    }

    public function timeEntries() {
        return $this->hasMany(TimeEntry::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function getProgressAttribute() {
        if ($this->estimated_time_in_mins) {
            return round(($this->time_spent_in_mins / $this->estimated_time_in_mins) * 100, 2);
        } else {
            return 0;
        }
    }

    public function getTaskPriorityAttribute() {
        $to = Carbon::now();
        $taskPrio = 'Low';
        if ($this->due_date) {
            $from = Carbon::parse($this->due_date);
            $diffInDays = $to->diffInDays($from);
            if ($diffInDays < 2 && $diffInDays > 0 && $from->gt($to)) {
                $taskPrio = 'Medium';
            } elseif ($diffInDays == 0 || Carbon::parse($this->due_date)->isPast() || $this->progress > 100) {
                $taskPrio = 'High';
            }
        }
        return $taskPrio;
    }

    public function getTaskDateAttribute() {
        $yesterday = Carbon::yesterday();
        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();
        $date = $today->eq($this->due_date) ? 'Today' : ($tomorrow->eq($this->due_date) ? 'Tomorrow' : ($yesterday->eq($this->due_date) ? 'Yesterday' : Carbon::parse($this->due_date)->format('M d')));
        return $date;
    }
}
