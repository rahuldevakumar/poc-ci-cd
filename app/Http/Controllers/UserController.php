<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use App\Models\User;
use PortalAPIHelper;

class UserController extends Controller
{
    /**
     * Render view of user settings page
     *
     * @param  \Illuminate\Http\Requests\Request $request
     * @return \Illuminate\View\View|\Closure|string
     **/
    public function settings(Request $request)
    {
        $data['page_title'] = 'Settings';
        $data['user'] = Auth::user();
        return view('user.settings', $data);
    }

    /**
     * Udpdate the of settings of logged in user
     *
     * @param  \Illuminate\Http\Requests\Request $request
     * @return \Illuminate\View\View|\Closure|string
     **/
    public function updateSettings(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:255',
            'password' => ['nullable', 'confirmed', Rules\Password::defaults()],
            'portal_api_key' => 'required',
        ]);
        
        $user = User::find(auth()->id());
        $user->name = $request->name;
        if ($request->filled('password')) {
            $user->password = Hash::make($request->password);
        }
        
        if ($user->portal_api_key != $request->portal_api_key) {
            $user->portal_api_key = $request->portal_api_key;

            $portal = new PortalAPIHelper();
            $endpoint = "projects/api/v2/me.json";
            $portal->setApiKey($user->portal_api_key);
            $response = $portal->get($endpoint);
            
            if (! empty($response->person)) {
                $user->portal_user_id = $response->person->id;
            }
        }
        
        $user->save();
        
        return redirect()->route('user.settings')->with('success', 'User details updated successfully');
    }
}
