<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Comment;

class CommentController extends Controller
{
    /**
     * Fetch latest comments which contains the user's name
     * 
     * 
     */
    public function fetchComments() {
        $user = Auth::user();
        $data['comments'] = Comment::where('body', 'like', '%' . $user->name . '%')->orWhere('body', 'like', '%' . $user->username . '%')->orderBy('datetime', 'desc')->get();
        return view('comments.notification', $data);
    }

    /**
     * Delete Comment 
     * 
     */
    public function deleteComment(Request $request) {
        Comment::where('comment_id', $request->comment_id)->delete();
        return response()->json(['success' => 'Deleted Comment']);
    }
}
