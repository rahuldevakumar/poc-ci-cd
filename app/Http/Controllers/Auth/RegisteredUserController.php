<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

use App\Providers\RouteServiceProvider;

use PortalAPIHelper;

use App\Models\User;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'apikey' => 'required|string',
            'username' => 'required|string|max:255',
        ]);
        $portal = new PortalAPIHelper($request->apikey);
        $endpoint = "projects/api/v2/me.json";
        $response = $portal->get($endpoint, null, 'web');
        if (! is_object($response)) {
            return back()->withErrors(['apikey', 'Please check your Api Key and try again.']);
        }

        $user = User::where('email', $request->email)->first();
        if (! $user) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'portal_api_key' => $request->apikey,
                'avatar' => $response->person->avatarUrl,
                'portal_user_id' => $response->person->id,
                'username' => $request->username,
            ]);
        } else {
            $user->update(['name' => $request->name, 'password' => Hash::make($request->password), 'portal_api_key' => $request->apikey, 'avatar' => $response->person->avatarUrl, 'portal_user_id' => $response->person->id, 'username' => $request->username]);
        }

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
