<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\TaskAssignee;
use App\Models\Task;
use App\Models\Tag;
use DB;
use Auth;
use Carbon\Carbon;
use App\Models\TagGroupingKey;
use Illuminate\Database\Eloquent\Collection;

class DashboardController extends Controller
{
    /**
     * Dashboard page details
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data['page_title'] = 'Dashboard';
        $data['project_count'] = Project::select('projects.project_id')->join('tasks','projects.project_id', '=', 'tasks.project_id')
                                ->distinct('projects.project_id')->count();

        $data['projects'] = Project::select('projects.id','projects.name')->join('tasks','projects.project_id', '=', 'tasks.project_id')
        ->where('projects.skip_updation', false)
        ->distinct('projects.name')->get();

        $data['tags'] = Tag::all();

        $projectTag = $request->project_tag ?: 'none';
        $project = $request->projects;
        $startDate = '';
        $endDate = '';
        if ($request->due_date != 'none') {
            switch ($request->due_date) {
                case 'today': 
                    $startDate = Carbon::now()->format('Y-m-d');
                    $endDate = Carbon::now()->format('Y-m-d');
                    break;
                case 'tomorrow': 
                    $startDate = Carbon::tomorrow()->format('Y-m-d');
                    $endDate = Carbon::tomorrow()->format('Y-m-d');
                    break;
                case 'next_week': 
                    $startDate = Carbon::now()->addWeek()->startOfWeek()->format('Y-m-d');
                    $endDate = Carbon::now()->addWeek()->endOfWeek()->format('Y-m-d');
                    break;
                case 'next_month':
                    $startDate = Carbon::now()->addMonth()->startOfMonth()->format('Y-m-d');
                    $endDate = Carbon::now()->addMonth()->endOfMonth()->format('Y-m-d');
                    break;
                default:
                    $startDate = '';
                    $endDate = '';
            }
        }  else {
            $startDate = $request->start_date ?: '';
            $endDate = $request->end_date ?: '';
        }
        $taskProgress = '';
        if ($request->task_progress != 'all' && array_key_exists($request->task_progress, config('task.progress'))) {
            $taskProgress = config('task.progress')[$request->task_progress];
        }

        $data['tag_grouping_keys'] = TagGroupingKey::with(['tags' => function ($q) use ($projectTag) {
            if ($projectTag != 'none') {
                return $q->where('tag_id', $projectTag);
            } 
        },'tags.projects' => function ($q) use ($project) {
            if ($project) {
                $q = $q->where('projects.id', $project);
            }
            return $q->where('projects.skip_updation',false);
        }, 'tags.projects.tasks' => function ($q) use ($startDate, $endDate, $taskProgress) {
            if ($startDate && $endDate) {
                $q = $q->whereBetween('due_date', [$startDate, $endDate]);
            } elseif ($startDate) {
                $q = $q->where('due_date', '>=', $request->start_date);
            } elseif ($endDate) {
                $q = $q->where('due_date', '<=', $request->end_date);
            }

            if ($taskProgress) {
                $q = $q->whereBetween('tasks.progress', $taskProgress);
            }
            return $q->where(function ($query) {
                $query->whereNotNull('tasks.due_date')->orWhereHas('comments');
            })->whereHas('taskTags', function ($th) {
                return $th->where('skip',false);
            })
            ->where('tasks.status', '!=', 'completed')->orderBy('tasks.progress', 'desc');
        }])->get();

        $data['filters'] = $request;
        $data['html'] = view('partial.task_listing', $data)->render();

        return view('home.dashboard', $data);
    }

    /**
     * Add a task to To Do list
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addToDo(Request $request) {
        $task = TaskAssignee::where('portal_user_id', Auth::user()->portal_user_id)->where('portal_task_id', $request->task_id)->first();
        $task->update(['to_do_flag' => 1]);

        return response()->json(['success'=>'Ajax request submitted successfully']);
    }

    /**
     * Remove a task from To Do list
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeToDo(Request $request) {
        $task = TaskAssignee::where('portal_user_id', Auth::user()->portal_user_id)->where('portal_task_id', $request->task_id)->first();
        $task->update(['to_do_flag' => 0]);
        return response()->json(['success'=>'Ajax request submitted successfully', 'task' => $task]);
    }

    /**
     * Retrieve all tasks present in To Do list
     * 
     * @return \Illuminate\Http\Response
     */
    public function toDo() {
        $todos = TaskAssignee::select('projects.project_id', 'projects.name AS project_name', 'tasks.task_id', 'tasks.name')
        ->join('tasks', 'task_assignees.portal_task_id', '=', 'tasks.task_id')
        ->join('projects','projects.project_id', '=', 'tasks.project_id')
        ->where('task_assignees.to_do_flag', 1)
        ->where('task_assignees.portal_user_id', Auth::user()->portal_user_id)
        ->orderBy('project_name')->get();
        return response()->json(['success' => 'Retrieved todos successfully', 'tasks' => $todos]);
    }

    /**
     * Get task priority: Low, Medium, High
     * 
     * @param  App\Models\Task $task
     * @return $taskPrio
     */
    public function taskPriority($task) {
        $to = Carbon::now();
        $taskPrio = 'Low';
        if ($task->due_date) {
            $from = Carbon::parse($task->due_date);
            $diffInDays = $to->diffInDays($from);
            if ($diffInDays < 2 && $diffInDays > 0 && $from->gt($to)) {
                $taskPrio = 'Medium';
            } elseif ($diffInDays == 0 || Carbon::parse($task->due_date)->isPast() || $task->progress > 100) {
                $taskPrio = 'High';
            }
        }
        return $taskPrio;
    }

    /**
     * Get task priority: Low, Medium, High
     * 
     * @param  App\Models\Task $task
     * @return $date
     */
    public function taskDate($task) {
        $yesterday = Carbon::yesterday();
        $today = Carbon::now();
        $tomorrow = Carbon::tomorrow();
        $date = $today->eq($task->due_date) ? 'Today' : ($tomorrow->eq($task->due_date) ? 'Tomorrow' : ($yesterday->eq($task->due_date) ? 'Yesterday' : Carbon::parse($task->due_date)->format('M d')));
        return $date;
    }

    /**
     * Sync Projects, Tasks, TimeEntries
     * 
     */
    public function syncTasks() {
        \Artisan::call('sync:projects');
        \Artisan::call('update:projectHours');
        \Artisan::call('sync:tasks');
        \Artisan::call('update:tasks');
        \Artisan::call('sync:time');
        \Artisan::call('sync:latestComments');
        return response()->json(['success' => 'Completed fetching data']);
    }
}
