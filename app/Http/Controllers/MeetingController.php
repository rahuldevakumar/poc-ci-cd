<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

use App\Repositories\GoogleCalendarRepository;

use App\Models\GoogleAccessToken;
use App\Models\GoogleCalendarMeeting;

class MeetingController extends Controller
{
    public function getAuthorizationCode() {
        $objGc = new GoogleCalendarRepository;
        $redirectUrl = $objGc->requestAuthorizationCode();
	    return redirect($redirectUrl);
    }

    public function index(Request $request) {
        if (GoogleAccessToken::where('user_id', Auth::user()->id)->exists()) {
            // GoogleCalendarMeeting::where('user_id', Auth::user()->id)->where('start')
        } elseif ($request->has('code')) {
            $objGc = new GoogleCalendarRepository;
            $response = $objGc->requestAccessToken($request->code);
            
            if (isset($response->error)) {
                abort(401);
            } 
            if (! isset($response->refresh_token)) {
                return view('meetings.remove_permission');
            }

            $token = new GoogleAccessToken;
            $token->user_id = Auth::user()->id;
            $token->access_token = $response->access_token;
            $token->expires_in = $response->expires_in;
            $token->refresh_token = $response->refresh_token;
            $token->save();

            return view('meetings.no_data');
        } else {
            return $this->getAuthorizationCode();
        }
        
    }
}
