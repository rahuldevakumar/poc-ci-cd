<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SyncUsers::class,
        Commands\SyncProjectCategories::class,
        Commands\SyncProjects::class,
        Commands\UpdateProjectHours::class,
        Commands\SyncTasks::class,
        Commands\UpdateTaskDetails::class,
        Commands\SyncTimeEntries::class,
        Commands\SyncLatestComments::class,
        Commands\FetchTimeLog::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sync:users')->daily();
        $schedule->command('sync:projectCategories')->daily();;
        $schedule->command('sync:projects')->daily();
        $schedule->command('update:projectHours')->everyTwoHours();
        $schedule->command('sync:tasks')->everyTwoHours();
        $schedule->command('update:tasks')->everyTwoHours();
        $schedule->command('sync:time')->everyTwoHours();
        $schedule->command('sync:latestComments')->everyTwoHours();
        $schedule->command('send:timeLogReport')->daily('14:30');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
