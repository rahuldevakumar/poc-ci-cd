<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use PortalAPIHelper;

use App\Models\Task;
use App\Models\TaskAssignee;
use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;

class SyncLatestComments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:latestComments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync latest comments of the past 24 hrs of the task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Comment::query()->delete();

        $portal = new PortalAPIHelper(config('portal.api_key'));
        $tasks = Task::whereDate('updated_at', '>', Carbon::now()->subDays(2))->get();
        $apiCallCount = 0; 
        foreach ($tasks as $task) {
            $this->info("Fetching comment of task with id ".  $task->task_id);
            $apiCallCount++;
            $endpoint = "v/2/tasks/".$task->task_id."/comments.json?page=1&pageSize=5&orderBy=date&sortOrder=desc";
            $options = [];
            $response = $portal->get($endpoint, $options);

            if (isset($response->comments)) {
                foreach($response->comments as $comment) {
                    if (Carbon::parse($comment->datetime) >= Carbon::now()->subDay()) {
                        $processedComment = trim( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', mb_convert_encoding( $comment->body, "UTF-8" ) ) );
                        $insertComment = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si",'<$1$2>', $processedComment);
                        $user = isset($comment->author) ? User::where('portal_user_id', $comment->author->id)->first() : '';
                        Comment::create(['comment_id' => $comment->id, 'task_id' => $task->id, 'body' => $insertComment, 'user_id' => $user ? $user->id : NULL, 'datetime' => Carbon::parse($comment->datetime)]);
                    }
                }
            } else {
                $this->info('Failed to retrieve comments');
            }
            if ($apiCallCount >= 80) {
                $this->info("Sleeping for 1 min");
                sleep(60);
                $apiCallCount = 0;
            }
        }
        $this->info('Retrieved comments successfully');
    }
}
