<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Project;
use App\Models\Task;
use App\Models\TaskTag;
use App\Models\TaskTaskTag;
use App\Models\User;
use App\Models\TaskAssignee;
use Carbon\Carbon;
use PortalAPIHelper;

class SyncTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync tasks from portal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info('Sync Tasks');
        $endpoint = "projects/api/v2/projects/0/tasks.json";
        $tasks_filters = ["all"];

        $apiCallCount = 0; 
        $portal = new PortalAPIHelper(config('portal.api_key'));
        foreach ($tasks_filters as $filter_type) {
            $this->info("Starting iteration for ". $filter_type);
            $page = 1;
            do {
                $this->info("Fetching ". $filter_type . " type - starting page ". $page);
                $options = $this->generateOptionsForTasks($filter_type, $page);
                $apiCallCount++;
                $response = $portal->get($endpoint, $options);
                if (! empty($response->tasks)) {
                    $this->info(count($response->tasks));
                    foreach($response->tasks as $task) {
                        $task_arr = [
                            'task_id' => $task->id,
                            'name' => $task->name,
                            'projectId' => $task->projectId,
                            'status' => $task->status,
                            'priority' => $task->priority,
                            'column' => isset($task->boardColumn) ? $task->boardColumn->name : '-',
                            'due_date' => $task->dueDate,
                            'tags' => ! empty($task->tags) ? $task->tags : [],
                            'assignedTo' => ! empty($task->assignedTo) ? $task->assignedTo : [],
                        ];

                        $existingTask = Task::where('task_id', '=', $task->id)->first();
                        if ($existingTask) {
                            Task::where('task_id', $task->id)
                                ->update([
                                    'project_id' => $task->projectId,
                                    'name' => $task->name,
                                    'status' => $task->status,
                                    'priority' => $task->priority,
                                    'details' => $task_arr,
                                    'due_date' => ! empty($task_arr['due_date']) ? Carbon::parse($task_arr['due_date']) : null,
                                    'last_modified_at' => Carbon::parse($task->dateLastModified)->format('Y-m-d H:i:s'),
                                ]);
                            TaskTaskTag::where('task_id', $existingTask->id)->delete();
                            $this->info("The task with ID: ".$task->id." has been updated");
                        } else {
                            $task_new = new Task;
                            $task_new->task_id = $task->id;
                            $task_new->project_id = $task->projectId;
                            $task_new->name = $task->name;
                            $task_new->status = $task->status;
                            $task_new->priority = $task->priority;
                            $task_new->details = $task_arr;
                            $task_new->due_date = ! empty($task_arr['due_date']) ? Carbon::parse($task_arr['due_date']) : null;
                            $task_new->last_modified_at = Carbon::parse($task->dateLastModified)->format('Y-m-d H:i:s');
                            $task_new->save();
                            $this->info("The task with ID: ".$task->id." has been created");
                        }

                        if (isset($task->tags)) {
                            foreach ($task->tags as $taskTag) {
                                $taskTag = TaskTag::firstOrCreate(['task_tag_id' => $taskTag->id], ['name' => $taskTag->name]);
                                TaskTaskTag::create(['task_id' => $existingTask ? $existingTask->id : $task_new->id, 'task_tag_id' => $taskTag->id]);
                            }
                        }

                        if (! empty($task->assignedTo)) {
                            $existingAssignees = TaskAssignee::where('portal_task_id', $task->id)->pluck('portal_user_id')->all();
                            $removeAssignees = [];
                            foreach ($task->assignedTo as $k => $assignee) {
                                if (! in_array($assignee->id, $existingAssignees) && count($existingAssignees)) {
                                    $removeAssignees[] = $assignee->id;
                                }

                                //create task assignees
                                $taskAssignee = TaskAssignee::where('portal_user_id', $assignee->id)->where('portal_task_id', $task->id)->first();
                                if (! $taskAssignee) {
                                    $objTaskAssignee = new TaskAssignee;
                                    $objTaskAssignee->portal_user_id = $assignee->id;
                                    $objTaskAssignee->portal_task_id = $task->id;
                                    $objTaskAssignee->save();
                                }
                            }
                            //remove assignees from the DB permanantly if already removed in portal
                            if (! empty($removeAssignees)) {
                                TaskAssignee::whereIn('portal_user_id', $removeAssignees)->where('portal_task_id', $task->id)->forceDelete();
                            }
                        }
                    }
                    $page++;
                }
                if ($apiCallCount >= 50) {
                    $this->info("Sleeping for 1 min");
                    sleep(60);
                    $apiCallCount = 0;
                }
            }
            while(count($response->tasks) == 250);
        }
        $this->info("Task list has been updated successfully");
    }

    private function generateOptionsForTasks($type, $page) {
        $options = [];
        $filter = 'started';
        switch($type) {
            case 'started':
                $filter = 'started';
                break;
            case 'late':
                $filter = 'overdue';
                break;
            case 'today':
                $filter = 'today';
                break;
            case 'upcoming':
                $filter = 'within14';
                break;
            default:
                $filter = 'all';
                break;
        }

        $projectIds = Project::where('skip_updation', false)->pluck('project_id')->toArray();

        $options = [
            "page" => $page,
            "pageSize" => 250,
            "useAllProjects" => true,
            "filter" => $filter,
            "sort" => "startdate",
            "sortOrder" => "desc",
            "includeToday" => 1,
            "today" => \Carbon\Carbon::now()->format("Ymd"),
            "includeCustomFields" => true,
            "projectStatus" => "active",
            "includeCompletedTasks" => true,
            "updatedAfterDate" => Carbon::now()->subMonth()->format('Ymd'),
            "projectIds" => $projectIds ? implode(',', $projectIds) : NULL,
        ];
        return $options;
    }
}
