<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\Models\GoogleAccessToken;

use App\Repositories\GoogleCalendarRepository;

class RefreshAccessToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh tokens which are expiring';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tokens = GoogleAccessToken::all();
        foreach ($tokens as $token) {
            $updatedAt = Carbon::parse($token->updated_at);
            $now = Carbon::now();
            // if ($now->diffInMinutes($updatedAt) <= 15) {
                $objGc = new GoogleCalendarRepository;
                $response = $objGc->refreshAccessToken($token->refresh_token);
                $token->update(['access_token' => $response->access_token, 'expires_in' => $response->expires_in]);
            // }
        }
        $this->info('Tokens have been updated successfully.');
    }
}
