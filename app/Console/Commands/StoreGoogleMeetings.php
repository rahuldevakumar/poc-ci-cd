<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use Carbon\Carbon;

use App\Repositories\GoogleCalendarRepository;

use App\Models\GoogleAccessToken;
use App\Models\GoogleCalendarMeeting;

class StoreGoogleMeetings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:meeting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store Google Meetings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tokens = GoogleAccessToken::all();
        $objGc = new GoogleCalendarRepository;
        foreach ($tokens as $token) {
            $meetings = 0;
            $meetings = GoogleCalendarMeeting::where('user_id', $token->user_id)->count();
            if ($meetings == 0) {
                $pageToken = null;
                do {
                    $events = $objGc->listEvents($token->access_token, $token->user->email, $pageToken);
                    foreach ($events->items as $event) {
                        $check = GoogleCalendarMeeting::where('unique_event_id', $event->id)->exists();
                        if (! $check) {
                            $meeting = new GoogleCalendarMeeting;
                            $meeting->user_id = $token->user_id;
                            $meeting->unique_event_id = $event->id;
                            $meeting->unique_calendar_id = $event->iCalUID;
                            $meeting->summary = $event->summary;
                            $meeting->html_link = $event->htmlLink;
                            $meeting->organizer_email = $event->organizer->email;
                            $meeting->start = isset($event->start->dateTime) ? Carbon::parse($event->start->dateTime)->format('Y-m-d H:i:s') : (isset($event->start->date) ? Carbon::parse($event->start->date)->format('Y-m-d H:i:s') : null);
                            $meeting->end = isset($event->end->dateTime) ? Carbon::parse($event->end->dateTime)->format('Y-m-d H:i:s') : (isset($event->end->date) ? Carbon::parse($event->end->date)->format('Y-m-d H:i:s') : null);
                            $meeting->save();
                        }
                    }
                    $pageToken = isset($events->nextPageToken) ? $events->nextPageToken : null;
                } while (isset($events->nextPageToken));
            } else {
                $pageToken = null;
                do {
                    $events = $objGc->listLatestEvents($token->access_token, $token->user->email, $pageToken);
                   //Need to add content
                    $pageToken = isset($events->nextPageToken) ? $events->nextPageToken : null;
                } while (isset($events->nextPageToken));
            }
        }
    }
}
