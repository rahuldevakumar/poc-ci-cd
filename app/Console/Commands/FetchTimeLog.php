<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;

use PortalAPIHelper;

use App\Models\UserHour;
use App\Models\Team;

class FetchTimeLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:timeLog {date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Time Logs of all employees daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $portal = new PortalAPIHelper(config('portal.api_key'));

        $currentDate = $this->argument('date') ? Carbon::parse($this->argument('date')) : Carbon::now();

        $teams = Team::where('display_time_logs',1)->get();

        $apiCallCount = 0; 
        foreach ($teams as $team) {
            foreach ($team->users as $user) {
                $this->info("Fetching time log of user with id ".  $user->portal_user_id);
                $apiCallCount++;
                $endpoint = "people/" . $user->portal_user_id . "/loggedtime.json?m=" . $currentDate->format('n') . "&y=" . $currentDate->format('Y');
                $options = [];
                $response = $portal->get($endpoint, $options);

                if (isset($response->user)) {
                    foreach($response->user->billable as $log) {
                        $date = date("Y-m-d", substr($log[0], 0, 10));
                        $userHour = UserHour::where('user_id', $user->id)->whereDate('date_of_log', $date)->first();
                        $userHour ? 
                            $userHour->update(['billable_hours' => $log[1]]) :
                            UserHour::create(['user_id' => $user->id, 'billable_hours' => $log[1], 'date_of_log' => $date]);
                    }

                    foreach($response->user->nonbillable as $log) {
                        $date = date("Y-m-d", substr($log[0], 0, 10));
                        $userHour = UserHour::where('user_id', $user->id)->whereDate('date_of_log', $date)->first();
                        $userHour ? 
                            $userHour->update(['non_billable_hours' => $log[1]]) :
                            UserHour::create(['user_id' => $user->id, 'non_billable_hours' => $log[1], 'date_of_log' => $date]);
                    }
                } else {
                    $this->info('Failed to retrieve time logs of employees');
                }
                if ($apiCallCount >= 80) {
                    $this->info("Sleeping for 1 min");
                    sleep(60);
                    $apiCallCount = 0;
                }
                
            }
        }
    }
}
