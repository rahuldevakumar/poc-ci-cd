<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\TimeEntry;
use App\Models\User;
use App\Models\Task;

use PortalAPIHelper;
use Carbon\Carbon;

class SyncTimeEntries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync time entries of tasks of latest 6 months';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('Sync time entries');
        $entries = TimeEntry::all();
        if (! $entries) {
            $this->fetchEntries(Carbon::now()->subMonths(6)->format('Y-m-d'));
        } else {
            $this->fetchEntries(Carbon::now()->subHours(2)->format('Y-m-d'));
            $this->deleteOldEntries();
        }
    }

    /**
     * Fetch entries based on the start date
     * 
     * @param
     * 
     */
    public function fetchEntries($startDate) {
        $portal = new PortalAPIHelper(config('portal.api_key'));
        
        $apiCallCount = 0; 
        $page = 1;
        do {
            $this->info("Started fetching time entries - page ". $page);
            $endpoint = "projects/api/v3/time.json";
            $options = [
                "page"                               => $page,
                "pageSize"                           => 250,
                "includeTotals"                      => true,
                "startDate"                          => $startDate,
                "endDate"                            => Carbon::now()->format('Y-m-d')        
            ];

            $response = $portal->get($endpoint, $options);
            $apiCallCount++;
            if (! empty($response->timelogs)) {
                foreach($response->timelogs as $entry) {
                    $existingTime = TimeEntry::where('time_entry_id', $entry->id)->first();
                    $user = User::where('portal_user_id', $entry->userId)->first();
                    $task = Task::where('task_id', $entry->taskId)->first();    
                    if ($task) {
                        if (! $existingTime) {
                            $time_new = new TimeEntry;
                            $time_new->time_entry_id = $entry->id;
                            $time_new->user_id = isset($user->id) ? $user->id : 0;
                            $time_new->is_billable = $entry->billable;
                            $time_new->hours = $entry->minutes / 60;
                            $time_new->date = Carbon::parse($entry->dateCreated)->format('Y-m-d H:i:s');
                            $time_new->description = ! empty($entry->description) ? $entry->description : '-';
                            $task->timeEntries()->save($time_new);
                            $this->info("The time entry with ID: ". $entry->id . " has been created");
                        } else {
                            TimeEntry::where('time_entry_id', $entry->id)
                                ->update([
                                    'is_billable'   => $entry->billable,
                                    'hours'         => $entry->minutes / 60,
                                    'description'   => ! empty($entry->description) ? $entry->description : '-',
                                    'date'          => Carbon::parse($entry->dateCreated)->format('Y-m-d H:i:s'),
                                    'task_id'       => Task::where('task_id', $entry->taskId)->first()->id,
                                ]);
                            $this->info("The time entry with ID: ". $entry->id. " has been updated");
                        }
                    }
                }
                $page++;
            }
            if ($apiCallCount >= 50) {
                $this->info("Sleeping for 1 min");
                sleep(60);
                $apiCallCount = 0;
            }
        } while(count($response->timelogs) == 250);
        $this->info("Time entries have been created and updated successfully");
    }

    public function deleteOldEntries() {
        TimeEntry::where('date', '<', Carbon::now()->subMonths(6))->delete();
        $this->info('Deleted time log entries older than 6 months');
    }
}
