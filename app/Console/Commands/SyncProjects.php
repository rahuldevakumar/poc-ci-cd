<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Project;
use App\Models\User;
use App\Models\Tag;
use App\Models\Category;
use App\Models\ProjectTag;
use Carbon\Carbon;
use PortalAPIHelper;

class SyncProjects extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:projects';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync projects data from portal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Inside Sync Projects");
        $portal = new PortalAPIHelper(config('portal.api_key'));
        
        $endpoint = "projects/api/v2/projects.json";

        $page = 1;
        do {
            $this->info("Started fetching projects - page ". $page);
            $options = [
                "page"                               => $page,
                "pageSize"                           => "250",
                "getPermissions"                     => "true",
                "getNotificationSettings"            => "false",
                "getDateInfo"                        => "true",
                "searchCompany"                      => "true",
                "formatMarkdown"                     => "false",
                "getActivePages"                     => "true",
                "includeUpdates"                     => "true",
                "includeProjectOwner"                => "true",
                "include"                            => "budgets",
                "returnLetters"                      => "true",
                "orderBy"                            => "starredcompanyname",
                "orderMode"                          => "desc",
                "includeCustomFields"                => "true",
                "status"                             => "active",
                "updatedAfterDate"                   => Carbon::now()->subMonth()->format('Ymd'),
            ];

            $response = $portal->get($endpoint, $options);
            if (! empty($response->projects)) {
                foreach($response->projects as $project) {
                    $project_arr = [
                        'id' => $project->id,
                        'name' => $project->name,
                        'status' => $project->status,
                        'company' => $project->company,
                        'owner' => ! empty($project->owner) ? $project->owner : []
                    ];

                    $existingProject = Project::where('project_id', $project->id)->first();
                    $category = isset($project->category->id) ? Category::where('category_id', $project->category->id)->first() : NULL;
                    if (! $existingProject) {
                        $project_new = new Project;
                        $project_new->project_id = $project->id;
                        $project_new->name = $project->name;
                        $project_new->status = $project->status;
                        $project_new->details = $project_arr;
                        $project_new->owner = ! empty($project_arr['owner']) ? $project_arr['owner']->fullName : '-';
                        $project_new->category_id = isset($category->id) ? $category->id : NULL;
                        $project_new->save();
                        $this->info("The project with ID: ". $project_new->id . " has been created");
                    } else {
                        Project::where('project_id', $project->id)
                            ->update([
                                'name' => $project->name,
                                'status' => $project->status,
                                'details' => $project_arr,
                                'owner' => ! empty($project_arr['owner']) ? $project_arr['owner']->fullName : '-',
                                'category_id'   => isset($category->id) ? $category->id : NULL,
                            ]);

                        ProjectTag::where('project_id', $existingProject->id)->delete();
                        
                        $this->info("The project with ID: ".$project->id." has been updated");
                    }
                    if (isset($project->tags)) {
                        foreach ($project->tags as $tag) {
                            $projectTag = Tag::where('tag_id', $tag->id)->first();
                            if (! $projectTag) {
                                $projectTag = Tag::create(['tag_id' => $tag->id, 'name' => $tag->name]);
                            }
                            $existingProject ? $existingProject->tags()->attach([$projectTag->id]) :$project_new->tags()->attach([$projectTag->id]);
                        }
                    }
                }
                $page++;
            }
        } while(count($response->projects) == 250);
        $this->info("Project list has been updated successfully");
    }
}
