<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use PortalAPIHelper;

use App\Models\User;
use App\Models\Team;


class SyncUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync details of Users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $portal = new PortalAPIHelper(config('portal.api_key'));
        $endpoint = "projects/api/v2/people.json";
        $page = 1;
        do {
            $this->info("Fetching users - starting page ". $page);
            $options = $this->generateOptionsForUsers($page);
            $response = $portal->get($endpoint, $options);
            $ids = array();
            if (! empty($response->people)) {
                foreach($response->people as $person) {
                    if ($person->companyId == WRG_PORTAL_COMPANY_ID) {
                        array_push($ids, $person->id);
                        $user = User::where('portal_user_id', $person->id)->first();
                        if ($user) {
                            User::where('portal_user_id', $person->id)
                                ->update([
                                    'name' => $person->firstName.' '.$person->lastName,
                                    'email' => $person->emailAddress,
                                    'avatar' => $person->avatarUrl,
                                ]);
                                $this->info("The user with ID: ".$person->id." has been updated");
                        } else {
                            $user = new User;
                            $user->name = $person->firstName.' '.$person->lastName;
                            $user->email = $person->emailAddress;
                            $user->avatar = $person->avatarUrl;
                            $user->portal_user_id = $person->id;
                            $user->save();
                            $team = Team::where('name','Dev')->first();
                            $user->teams()->attach($team->id);
                            $this->info("The user with ID: ".$person->id." has been created");
                        }

                        /**
                         * Set team assigned in teamwork portal
                         */
                        // if (isset($person->teams)) {
                        //     foreach($person->teams as $teamName) {
                        //         $team = Team::where('name', $teamName)->first();
                        //         if (! $team) {
                        //             $team = Team::create(['name' => $teamName]);
                        //         }

                        //         $user->teams()->attach($team->id);
                        //     }
                        // }
                    }
                }
                $page++;
            }
        } while(count($response->people) == 100);
        User::whereNotIn('portal_user_id', $ids)->delete();
        $this->info("User details have been updated successfully");
    }

    private function generateOptionsForUsers($page) {
        $options = [
            "page"              => $page,
            "pageSize"          => 100,
            "getCounts"         => true,
            "includeClockIn"    => true,
            "returnTeams"       => true,
            "sort"              => "firstName",
            "sortOrder"         => "desc",
            "type"              => "", 
            "searchTerm"        => "",
        ];
        return $options;
    }
}
