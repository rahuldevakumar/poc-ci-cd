#!/bin/bash

# Project directory on server for your project.
export WEB_DIR="/var/www/html/teamwork_stage"

# Your server user. Used to fix permission issue & install our project dependcies
export WEB_USER="ubuntu"

# Change directory to project.
cd $WEB_DIR

git pull origin staging

sudo php artisan down


# load .env file from AWS Systems Manager

#echo "Generating ENV files from AWS Parameter"



# change user owner to www-data & fix storage permission issues.
echo "changing user owner to ubuntu & fix storage permission issues"
sudo chown -R $WEB_USER:www-data .
sudo find . -type f -exec chmod 664 {} \;
sudo find . -type d -exec chmod 775 {} \;
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache

sudo cp -r ../secret-stage/.env .

# install composer dependcies
echo "Composer Install"
sudo -u $WEB_USER composer install --no-interaction

echo "Running PHP Artisans Commands"

#refresh autload files
sudo -u $WEB_USER composer dump-autoload --no-interaction

#clear aritsan cache
sudo -u $WEB_USER php artisan cache:clear

#clear config cache
sudo -u $WEB_USER php artisan config:clear

sudo -u $WEB_USER php artisan route:clear

sudo -u $WEB_USER php artisan view:cache

#Refresh migrations & seed data
php artisan migrate

# Fetch all WRG Employees
sudo -u $WEB_USER php artisan sync:users

# Fetch all Project Categories
sudo -u $WEB_USER php artisan sync:projectCategories

# Fetch all Projects
sudo -u $WEB_USER php artisan sync:projects

# Fetch all Project Hours
sudo -u $WEB_USER php artisan update:projectHours

# Sync tasks of all projects
sudo -u $WEB_USER php artisan sync:tasks

# Fetch more details of each task
sudo -u $WEB_USER php artisan update:tasks

# Sync time of tasks
sudo -u $WEB_USER php artisan sync:time

# Fetch comments posted in the past 24 hrs
sudo -u $WEB_USER php artisan sync:latestComments


sudo -u $WEB_USER php artisan up



