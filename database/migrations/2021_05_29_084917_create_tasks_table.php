<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->integer('task_id');
            $table->text('name');
            $table->integer('project_id');
            $table->string('priority', 200)->nullable();
            $table->string('status', 200)->nullable();
            $table->dateTime('due_date')->nullable();
            $table->tinyInteger('is_completed')->default(0);
            $table->longText('details')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
