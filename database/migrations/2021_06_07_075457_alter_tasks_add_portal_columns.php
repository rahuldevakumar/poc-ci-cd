<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTasksAddPortalColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->text('description')->nullable()->after('is_completed');
            $table->integer('time_spent_in_mins')->nullable()->after('description');
            $table->integer('estimated_time_in_mins')->nullable()->after('time_spent_in_mins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('time_spent_in_mins');
            $table->dropColumn('estimated_time_in_mins');
        });
    }
}
