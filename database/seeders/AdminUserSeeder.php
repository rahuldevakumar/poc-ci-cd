<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email' => 'greg@whiterabbit.group',
                'password' => 'Tp9VfjGFtW',
            ],
            [
                'email' => 'divya.suseelan@whiterabbit.group',
                'password' => 'CfSiepncJU',
            ]
        ];
        
        foreach ($users as $user) {
            User::where('email', $user['email'])->update(['password' => Hash::make($user['password'])]);
        }
    }
}
