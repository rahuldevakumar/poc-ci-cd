<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddProjectsToSkipUpdationListSeeder::class);
        $this->call(UpdateTagOrderSeeder::class);
        $this->call(AdminUserSeeder::class);
        $this->call(CreateTeamAndAssignTeamMemberSeeder::class);
        $this->call(TimeLogTagSeeder::class);
        $this->call(SkipTaskTagSeeder::class);
    }
}
