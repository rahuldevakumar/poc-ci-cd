<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TaskTag;

class SkipTaskTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            "Complete",
            "Backlog",
            "Backlog / Hold",
            "Billed / Closed",
            "In Review and/or Live",
        ];
        foreach ($tags as $tag) {
            $row = TaskTag::where('name', $tag)->first();
            if ($row) {
                $row->update(['skip' => true]);
            }
        }
    }
}
