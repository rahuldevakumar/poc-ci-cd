<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TimeLogTag;

class TimeLogTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            [
                'name' => '[REWORK]',
                'description' => 'Rework due to developer mistake.',
            ],
            [
                'name' => '[CR]',
                'description' => 'Rework because of the client change request.',
            ],
            [
                'name' => '[DISCUSSION]',
                'description' => 'General discussion, Project discussion.',
            ],
            [
                'name' => '[KT]',
                'description' => 'Knowledge Transfer.',
            ],
            [
                'name' => '[DT]',
                'description' => 'Discovery time.',
            ],
            [
                'name' => '[CT]',
                'description' => 'Case Study based on the project.',
            ],
            [
                'name' => '[LRN]',
                'description' => 'Learning a new platform based on the project.',
            ],
            [
                'name' => '[SUPPORT]',
                'description' => 'Support on projects due to unresolved issues.',
            ],
            [
                'name' => '[QS]',
                'description' => 'QA Support',
            ],
            [
                'name' => '[DEV]',
                'description' => 'Normal Development.',
            ],
        ];

        foreach ($tags as $tag) {
            TimeLogTag::firstOrCreate([
                'name' => $tag['name'],
                'description' => $tag['description'],
            ]);
        }
        
    }
}
