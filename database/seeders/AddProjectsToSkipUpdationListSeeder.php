<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Project;

class AddProjectsToSkipUpdationListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projectIds = [
            243573, 
            443660,
            354070, 
            439614, 
            256381, 
            347470, 
            317498, 
            581582, 
            335983,
        ];

        foreach ($projectIds as $id) {
            Project::where('project_id', $id)->update(['skip_updation' => 1]);
        }
    }
}
