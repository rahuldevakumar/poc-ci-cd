<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PortalController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserHourController;
use App\Http\Controllers\MeetingController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect('login');
});

Route::middleware(['auth:web'])->group( function(){
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/tasks', [DashboardController::class, 'getTasks']);

    Route::group(['prefix' => 'settings'], function(){
        Route::get('/', [UserController::class, 'settings'])->name('user.settings');
        Route::post('/', [UserController::class, 'updateSettings'])->name('user.settings.update');
    });

    Route::get('/todo', [DashboardController::class, 'toDo']);
    Route::post('/add-todo', [DashboardController::class, 'addToDo']);
    Route::post('/remove-todo', [DashboardController::class, 'removeToDo']);
    Route::post('/sync-tasks', [DashboardController::class, 'syncTasks']);

    Route::get('/userHours', [UserHourController::class, 'fetchHours']);

    Route::get('/code', [MeetingController::class, 'getAuthorizationCode']);
    Route::get('/meetings', [MeetingController::class, 'index']);

    Route::post('/delete-comment', [CommentController::class, 'deleteComment']);

});

require __DIR__.'/auth.php';
